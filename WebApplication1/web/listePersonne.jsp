<%-- 
    Document   : listePersonne
    Created on : 18 nov. 2022, 20:31:18
    Author     : Mangatiana
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Personne"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <% ArrayList<Personne> liste = (ArrayList<Personne>) request.getAttribute("liste");%>
    <table>
        <% for(int i = 0; i < liste.size();i++) { %>
        <tr>
            <td><% out.print(liste.get(i).getNom()); %></td>
        </tr>
        <% } %>
    </table>
</html>
