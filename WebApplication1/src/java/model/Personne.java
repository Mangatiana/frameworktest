/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import utilitaire.Path;

/**
 *
 * @author Mangatiana
 */
public class Personne {

    private String nom;

    public Personne(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Path(url = "insert_personne")
    public ModelView insertPersonne() {
        ModelView m = new ModelView();
        m.addToAssoc("personne", this);
        m.setRedirection("success.jsp");
        return m;
    }

    @Path(url = "getAll_personne")
    public ModelView getAll() {
        ModelView m = new ModelView();
        ArrayList<Personne> ls = new ArrayList<>();
        ls.add(new Personne("Mangatiana"));
        ls.add(new Personne("Rando"));
        m.addToAssoc("liste", ls);
        m.setRedirection("listePersonne.jsp");
        return m;
    }

}
